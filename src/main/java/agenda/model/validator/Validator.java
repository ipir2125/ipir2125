package agenda.model.validator;

import agenda.model.base.User;

import java.util.List;

public interface Validator<E>{

	void validate(E entity, List<String> list);
	
}
