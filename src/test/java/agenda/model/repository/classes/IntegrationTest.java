package agenda.model.repository.classes;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IntegrationTest {
    private RepositoryActivity repAct;
    private RepositoryContact repCon;
    private Contact contact;
    private Activity act;

    @Before
    public void setup() throws Exception {
        repCon = new RepositoryContactFile();
        repAct = new RepositoryActivityFile(repCon);

        for (Activity a : repAct.getActivities())
            repAct.removeActivity(a);
    }

    @Test
    public void a1addContactEC_TC1() {
        try {
            contact = new Contact("Paul", "Oradea", "+4012302141024", "email");
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        repCon.addContact(contact);
        for (Contact c : repCon.getContacts()) {
            if (c.equals(contact)) {
                assertTrue(true);
                break;
            }
        }
    }

    @Test
    public void b1addActivityWTC1() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            act = new Activity("",
                    df.parse("04/23/2019 12:00"),
                    df.parse("04/23/2019 13:00"),
                    null,
                    "descr");
            assertFalse(repAct.addActivity(act));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void c1testCase1Afis() {
        for (Activity act : repAct.getActivities())
            repAct.removeActivity(act);

        Calendar c = Calendar.getInstance();
        c.set(2019, 4, 14, 12, 00);
        Date start = c.getTime();

        c.set(2019, 4, 14, 12, 30);
        Date end = c.getTime();

        Activity act = new Activity("name1", start, end,
                new LinkedList<Contact>(), "description1");

        repAct.addActivity(act);

        c.set(2019, 4, 14);

        List<Activity> result = repAct.activitiesOnDate("name1", c.getTime());
        assertTrue(result.size() == 1);
    }


    @Test
    public void bigbangint() {
        a1addContactEC_TC1();
        b1addActivityWTC1();
        c1testCase1Afis();
    }

//    @Test
//    public void bigbangint() {
//        try {
//            contact = new Contact("Paul", "Oradea", "+4012302141024", "email");
//        } catch (InvalidFormatException e) {
//            assertTrue(false);
//        }
//        repCon.addContact(contact);
//        for (Contact c : repCon.getContacts()) {
//            if (c.equals(contact)) {
//                contact = c;
//                break;
//            }
//        }
//
//        for (Activity act : repAct.getActivities())
//            repAct.removeActivity(act);
//
//        Calendar c = Calendar.getInstance();
//        c.set(2019, 4, 14, 12, 00);
//        Date start = c.getTime();
//
//        c.set(2019, 4, 14, 12, 30);
//        Date end = c.getTime();
//
//        LinkedList<Contact> list = new LinkedList<Contact>();
//        list.add(contact);
//
//        Activity act = new Activity("name1", start, end, list, "description1");
//
//        repAct.addActivity(act);
//
//        c.set(2019, 4, 14);
//
//        List<Activity> result = repAct.activitiesOnDate("name1", c.getTime());
//        assertTrue(result.get(0).getName().equals("name1") && result.get(0).getContacts().get(0).getName().equals("Paul"));
//    }
}