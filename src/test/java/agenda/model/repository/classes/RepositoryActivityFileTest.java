package agenda.model.repository.classes;

import agenda.model.base.Activity;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import agenda.model.repository.interfaces.RepositoryUser;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.Assert.*;

public class RepositoryActivityFileTest {
    private Activity act;
    private RepositoryActivity rep;

    @Before
    public void setUp() throws Exception {
        RepositoryContact contactRep = new RepositoryContactFile();
        rep = new RepositoryActivityFile(
                contactRep);
    }

    @Test
    public void addActivityWTC1() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            act = new Activity("",
                    df.parse("04/23/2019 12:00"),
                    df.parse("04/23/2019 13:00"),
                    null,
                    "descr");
            assertFalse(rep.addActivity(act));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addActivityWTC2() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            act = new Activity("name1",
                    df.parse("04/23/2019 12:00"),
                    df.parse("04/23/2019 13:00"),
                    null,
                    "test1");
            rep.addActivity(act);
            act = new Activity("name1",
                    df.parse("04/23/2019 12:30"),
                    df.parse("04/23/2019 13:00"),
                    null,
                    "descr");
            assertFalse(rep.addActivity(act));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addActivityWTC3() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            act = new Activity("name1",
                    df.parse("03/20/2011 12:00"),
                    df.parse("03/20/2011 13:00"),
                    null,
                    "descr");

            assertTrue(rep.addActivity(act));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addActivityWTC4() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        for (Activity activity : rep.getActivities()) {
            rep.removeActivity(activity);
        }
        try {
            act = new Activity("name1",
                    df.parse("03/20/2010 12:00"),
                    df.parse("03/20/2010 13:00"),
                    null,
                    "descr");

            assertTrue(rep.addActivity(act));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}