package agenda.model.repository.classes;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RepositoryContactFileTest2 {
    private RepositoryContactFile repositoryContact;
    private Contact contact;

    @Before
    public void setUp() throws Exception {
        repositoryContact = new RepositoryContactFile();
    }


    @Test
    public void addContactEC_TC1() {
        try {
            contact = new Contact("Paul", "Oradea", "+4012302141024", "email");
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        repositoryContact.addContact(contact);
        for (Contact c : repositoryContact.getContacts()) {
            if (c.equals(contact)) {
                assertTrue(true);
                break;
            }
        }
    }

    @Test
    public void addContactEC_TC2() {
        // // // nu poate fi testat
//        try {
//            contact = new Contact(1245, 12123, 745544586);
//        } catch (InvalidFormatException e) {
//            assertTrue(false);
//        } catch (Exception error){
//            assertTrue(true);
//        }
//        repositoryContact.addContact(contact);
//        for (Contact c : repositoryContact.getContacts()) {
//            if (c.equals(contact)) {
//                assertTrue(true);
//                break;
//            }
//        }
    }

    @Test
    public void addContactEC_TC3() {
        try {
            contact = new Contact("Paul.Ivan", "Oradea", "+4012302141024", "email");
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        repositoryContact.addContact(contact);
        for (Contact c : repositoryContact.getContacts()) {
            if (c.equals(contact)) {
                assertTrue(true);
                break;
            }
        }
    }

    @Test
    public void addContactEC_TC4() {
        try {
            contact = new Contact("Paul.Ivan.First", "Oradea", "+4012302141024", "email");
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
    }

    @Test
    public void addContactBVA_TC1() {
        //""	"A…123"	"+40345532"
        try {
            contact = new Contact("",
                    "Oradea", "+4012302141024", "email");
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
    }

    @Test
    public void addContactBVA_TC3() {
        //"name"	"1"	"+40345532"
        try {
            contact = new Contact("name",
                    "1", "+4012302141024", "email");
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        repositoryContact.addContact(contact);
        for (Contact c : repositoryContact.getContacts()) {
            if (c.equals(contact)) {
                assertTrue(true);
                break;
            }
        }
    }

    @Test
    public void addContactBVA_TC4() {
        //"name"	"A…123"	"+40345532"
        try {
            contact = new Contact("name",
                    "1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",
                    "+4012302141024",
                    "email");
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
    }


}